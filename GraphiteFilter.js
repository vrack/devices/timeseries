
const { Device, Rule, Port } = require('vrack-core')
module.exports = class extends Device {
  description = 'Фильтрует метрики по маске регулярного выражения'

  ports () {
    return [
      new Port('metric').input().data('metric')
        .description('Timeseries метрика'),
      new Port('included').output().data('metric')
        .description('Отфильтрованные метрики'),
      new Port('excluded').output().data('metric')
        .description('Метрики не прошедшие фильтр')
    ]
  }

  checkParams () {
    return [
      new Rule('include')
        .required()
        .default([])
        .isArray()
        .description('Массив масок для фильтации')
    ]
  }

  settings () {
    return {
      message: false,
      shares: false
    }
  }

  regs = []

  process () {
    for (const rule of this.params.include) this.regs.push(new RegExp(rule))
  }

  inputMetric (data) {
    for (const reg of this.regs) {
      if (data[2].match(reg) === null) continue
      this.outputs.included.push(data)
      return
    }
    this.outputs.excluded.push(data)
  }
}
