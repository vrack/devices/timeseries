var net = require('net')
const { Device, Rule, Port } = require('vrack-core')
module.exports = class extends Device {
  description = 'Принимает метрики в формате graphite (metric.name value timestamp) и отправляет на выход `metric` в формате timeseris метрики'

  ports () {
    return [
      new Port('metric').output().data('metric')
        .description('Timeseries метрика')
    ]
  }

  checkParams () {
    return [
      new Rule('host').required().default('127.0.0.1').isString()
        .description('Хост сервера приема сообщений'),
      new Rule('port').required().default(2003).isInteger()
        .description('Порт сервера приема сообщений')
    ]
  }

  settings () {
    return { message: false }
  }

  shares = {
    clients: 0,
    received: 0
  }

  index = 0;

  buffer = {}
  timers = {}

  process () {
    var server = net.createServer((socket) => {
      this.shares.clients++
      this.render()
      socket._id = this.getNewId()
      socket.on('close', () => {
        this.shares.clients--
        delete this.buffer[socket._id]
        delete this.timers[socket._id]
        this.render()
      })
      socket.on('data', (data) => {
        const strData = data.toString('utf8')
        if (strData.slice(-1) !== '\n') return this.updateBuffer(socket._id, strData)
        const rows = strData.split('\n')
        for (const row of rows) {
          const acts = row.split(' ')
          if (acts.length === 3) {
            this.shares.received++
            this.outputs.metric.push([parseInt(acts[2]), parseFloat(acts[1]), acts[0]])
          } else {
            continue
          }
        }
        this.clearBuffer(socket._id)
        this.render()
      })
    })

    server.listen(this.params.port, this.params.host)
  }

  clearBuffer (id) {
    this.buffer[id] = ''
  }

  updateBuffer (id, data) {
    this.buffer[id] = this.buffer[id] + data
    this.bufferTimer(id)
  }

  bufferTimer (id) {
    if (this.timers[id]) clearTimeout(this.timers[id])
    setTimeout(() => {
      this.buffer[id] = ''
      this.timers[id] = false
    }, 3000)
  }

  getNewId () {
    return ++this.index
  }
}
