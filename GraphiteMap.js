const { Device, Port, Rule } = require('vrack-core')

module.exports = class extends Device {
  description = 'Принимает метрики graphite и строит карту метрик'

  ports () {
    return [
      new Port('metric').input().data('metric')
        .description('Timeseries метрика')
    ]
  }

  checkParams () {
    return [
      new Rule('renderTimeout').required().default(5000).isInteger().expression('value > 0')
        .description('Таймаут для обновления отправки данных'),
      new Rule('cleanTimeout').required().default(60000).isInteger().expression('value > 0')
        .description('Таймаут для обновления отправки данных')
    ]
  }

  settings () {
    return { message: false }
  }

  shares = {
    metrics: {}
  }

  process () {
    setInterval(this.render.bind(this), this.params.renderTimeout)
    setInterval(this.cleanMetrics.bind(this), this.params.cleanTimeout)
  }

  cleanMetrics () {
    const now = (Date.now() / 1000) - this.params.cleanTimeout
    for (var mn in this.shares.metrics) {
      if (this.shares.metrics[mn].time < now) delete this.shares.metrics[mn]
    }
    this.render()
  }

  inputMetric (data) {
    this.shares.metrics[data[2]] = { value: data[1], time: data[0] }
  }
}
