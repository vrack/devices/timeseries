const VRack = require('vrack-core')
const Rule = VRack.Rule
const Port = VRack.Port
module.exports = class extends VRack.Device {
  ports () {
    return [
      new Port('metric').input().data('metric')
        .description('Timeseries метрика'),
      new Port('metric').output().data('metric')
        .description('Timeseries метрика')

    ]
  }

  settings () {
    return {
      message: false,
      shares: false
    }
  }

  checkParams () {
    return [
      new Rule('list').default({}).isObject().required().example({
        'old.name.to.1': 'metric.name.to.1',
        'old.name.to.2': 'metric.name.to.2',
        'old.name.to.3': 'metric.name.to.3'
      })
        .description('Объект - аналог ассоциативного массива, для переназначения названий метрик')
    ]
  }

  inputMetric (data) {
    var nData = data.slice()
    if (this.params.list[nData[2]]) nData[2] = this.params.list[nData[2]]
    this.outputs.metric.push(nData)
  }
}
