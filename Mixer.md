Создает timeseries метрику на основе входящего числового значения

Для формирования уникального идентификатора можно использовать параметр `list` или использовать шаблон. Для использования шаблона необходимо назначить `true` в параметр `useTemplate` и указать шаблон `template` (по умолчанию `metric.{channel}`). Подстрока в шаблоне `{channel}` будет заменена на номер канала.

Входы FX могут использоватся для создание микшера микшеров метрик, приходящие на вход FX метрики будут отправляться на основной выход `metric`.

Выходы FX могут использоватся только вместе с роутингом. Роутинг позволяет отправить определенные основные входы на несколько FX выходов.

Пример указания роутинга:

    "routes": {
      "1": ["fx1","fx2"]
    }

Такие настройки роутинга позволят перенаправить вход 1 на выходы fx1 и fx2. Допускается указывать так же выход `metric` если нужно отправить оригинальную метрику на общий выход. 

**Необходимо учитывать, что если роутинг настроен неправильно и указывает на несуществующие входы или выходе - дашборд не будет запущен.**