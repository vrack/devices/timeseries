
const VRack = require('vrack-core')
const Rule = VRack.Rule
const Port = VRack.Port
module.exports = class extends VRack.Device {
  description = 'Фильтрует метрики по идентификатору'

  ports () {
    return [
      new Port('metric').input().data('metric')
        .description('Timeseries метрика'),
      new Port('included').output().data('metric')
        .description('Отфильтрованные метрики'),
      new Port('excluded').output().data('metric')
        .description('Метрики не прошедшие фильтр')
    ]
  }

  checkParams () {
    return [
      new Rule('include')
        .required()
        .default([])
        .isArray()
        .description('Массив метрик для фильтации')
    ]
  }

  settings () {
    return {
      message: false,
      shares: false
    }
  }

  inputMetric (data) {
    if (
      this.params.include.indexOf(data[2]) !== -1
    ) this.outputs.included.push(data)
    else this.outputs.excluded.push(data)
  }
}
