const VRack = require('vrack-core')
const Rule = VRack.Rule
const Port = VRack.Port
module.exports = class extends VRack.Device {
  description = 'Преобразовывает трехпортовое подключение в формат timeseries метрики. Параметры имеют аналогичное значения что и в `timeseries.Mixer`'
  ports () {
    return [
      new Port('unit').input().data('number')
        .description('Значение timeseries метрики'),
      new Port('channel').input().data('metric')
        .description('Канал'),
      new Port('gate').input().data('signal')
        .description('Сигнальный вход'),
      new Port('metric').output().data('metric')
        .description('Выход timeseries метрик'),
      new Port('gate').output().data('number')
        .description('Сигнальный выход')
    ]
  }

  checkParams () {
    return [
      new Rule('list').default({}).isObject().example({
        1: 'metric.name.to.1',
        2: 'metric.name.to.2',
        3: 'metric.name.to.3'
      })
        .description('Объект который позволяет назначить название метрик для обычных входов'),
      new Rule('useTemplate').required().default(false).isBoolean()
        .description('Если `true` то для названий метрик будет использоватся текстовый шаблон'),
      new Rule('template').required().default('metric.{channel}').isString()
        .description('Текстовый шаблон для названия метрик'),
      new Rule('inSeconds').required().default(true).isBoolean()
        .description('Если `true` время метрики будет указыватся в секундах, иначе в мс.')
    ]
  }

  settings () {
    return {
      message: false,
      shares: false
    }
  }

  inputUnit (data) {
    this.unit = data
  }

  inputChannel (data) {
    this.channel = data
  }

  inputGate (data) {
    this.outGate(this.channel, this.unit, data)
  }

  templateReg = /{(.*?)}/g

  outGate (channel, data, gate) {
    var metric = channel
    if (this.params.list[channel] !== undefined && !this.params.useTemplate) metric = this.params.list[channel]
    if (this.params.useTemplate) {
      var pms = { channel }
      metric = this.params.template.replace(this.templateReg, (entry, word) => pms[word])
    }
    this.outputs.metric.push([this.time(), data, metric])
    this.outputs.gate.push(gate)
  }

  time () {
    if (this.params.inSeconds) return Math.floor(Date.now() / 1000)
    else return Date.now()
  }
}
