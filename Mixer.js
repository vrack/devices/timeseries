const VRack = require('vrack-core')
const Rule = VRack.Rule
const Port = VRack.Port

module.exports = class extends VRack.Device {
  ports () {
    return [
      new Port('mix%d').input().data('number').dynamic(this.params.inputs)
        .description('Значение timeseries метрики'),
      new Port('fx%d').input().data('metric').dynamic(this.params.inputsFX)
        .description('Вход FX метрик'),
      new Port('metric').output().data('metric')
        .description('Выход timeseries метрик'),
      new Port('fx%d').output().data('number').dynamic(this.params.outputsFX)
        .description('Выход FX метрик')
    ]
  }

  checkParams () {
    return [
      new Rule('inSeconds').required().default(true).isBoolean()
        .description('Если `true` время метрики будет указыватся в секундах, иначе в мс.'),
      new Rule('inputs').default(0).isInteger().expression('value >= 0').example(3)
        .description('Количество основных числовых входов микшера'),
      new Rule('inputsFX').default(0).isInteger().expression('value >= 0')
        .description('Количество FX входов'),
      new Rule('outputsFX').default(0).isInteger().expression('value >= 0')
        .description('Количество FX выходов'),
      new Rule('routes').default({}).isObject()
        .description('Правила роутинга'),
      new Rule('list').default({}).isObject().example({
        1: 'metric.name.to.1',
        2: 'metric.name.to.2',
        3: 'metric.name.to.3'
      })
        .description('Объект который позволяет назначить название метрик для обычных входов'),
      new Rule('useTemplate').required().default(false).isBoolean()
        .description('Если `true` то для названий метрик будет использоватся текстовый шаблон'),
      new Rule('template').required().default('metric.{channel}').isString()
        .description('Текстовый шаблон для названия метрик')
    ]
  }

  settings () {
    return {
      message: false,
      shares: false
    }
  }

  templateReg = /{(.*?)}/g;

  preProcess () {
    for (let i = 1; i <= this.params.inputs; i++) this['inputMix' + (i)] = data => { this.outMix(i, data) }
    for (let i = 1; i <= this.params.inputsFX; i++) this['inputFx' + (i)] = data => { this.outFX(i, data) }
  }

  process () {
    for (var key in this.params.routes) {
      var aValue = this.params.routes[key]
      if (this.inputs['mix' + key] === undefined) throw new Error('Mixer route error - input port mix' + key + ' not found')
      for (var value of aValue) if (this.outputs[value] === undefined) throw new Error('Mixer route error - output port ' + value + ' not found')
    }
  }

  async outFX (i, data) {
    this.findList(this.params.listFX, data)
    this.outputs.metric.push(data)
  }

  async outMix (channel, data) {
    var nData = [this.time(), data, channel]
    if (this.params.useTemplate) this.useTemplate(nData)
    else this.findList(this.params.list, nData)
    this.pushRoute(channel, nData)
  }

  useTemplate (data) {
    var pms = { channel: data[2] }
    data[2] = this.params.template.replace(this.templateReg, (entry, word) => pms[word])
  }

  findList (list, data) {
    if (list && list[data[2]]) data[2] = list[data[2]]
  }

  pushRoute (channel, data) {
    if (!this.params.routes || !this.params.routes[channel]) return this.outputs.metric.push(data)
    for (var port of this.params.routes[channel]) this.outputs[port].push(data)
  }

  time () {
    if (this.params.inSeconds) return Math.floor(Date.now() / 1000)
    else return Date.now()
  }
}
